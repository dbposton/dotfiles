#!/bin/bash                                                                                                                                                                                                    
#                                                                                                                                                                                                  
#Establish Dotfiles Directory                                                                                                                                                                                  
#                                                                                                                                                                                                              
#Create Directories
mkdir -p ~/projects/go/{bin,pkg,src}
mkdir .ssh
#
#Clone Repositories
git clone https://github.com/dracula/vim ~/.vim/pack/plugins/start/dracula
git clone https://gitlab.com/prov47tech/dotfiles ~/projects/dotfiles
#git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go
#Symlink the files                                                                                                                                                                                             
#                                                                                                                                                                                                              
ln -sfv /root/projects/dotfiles/gitconfig ~/.gitconfig                                                                                                            
ln -sfv /root/projects/dotfiles/gitignore ~/.gitignore                                                                                                                           
ln -sfv /root/projects/dotfiles/vimrc ~/.vimrc                                                                                                                                            
ln -sfv /root/projects/dotfiles/config ~/.ssh/config                                                                                                                                                           
ln -sfv /root/projects/dotfiles/tmux.conf ~/.tmux.conf
# 
#Set additional enviromental variables
#                                                                                                                                                                                                             
export GOPATH=$HOME/projects/go         
export PATH=$PATH:$GOPATH/bin
#
#Install vim-go
#
#vim -esN +GoInstallBinaries +q
# 
#Install Prezto
#
/usr/bin/zsh <<'EOF'
git clone --recursive https://gitlab.com/prov47tech/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
setopt EXTENDED_GLOB
for rcfile in "${ZDOTDIR:-$HOME}"/.zprezto/runcoms/^README.md(.N); do
ln -s "$rcfile" "${ZDOTDIR:-$HOME}/.${rcfile:t}"
done
chsh -s /bin/zsh
EOF